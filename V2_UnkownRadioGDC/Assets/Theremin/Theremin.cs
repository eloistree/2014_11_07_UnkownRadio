﻿/**
 * HOW TO USE
 * Put the script on a gameObject
 * Theremin.instance.axis // Vector2 between 0 and 1
**/
using UnityEngine;
using System.Collections;
using System;
using Uniduino;
public class Theremin : MonoBehaviour
{

	private Arduino arduino;

	public int pin = 0;
	public int pinValue;


	private GUIStyle currentStyle = null;
	public Color simulateSquareColor = new Color( 0f, 1f, 0f, 0.8f );
	public bool simulate = true;
	public bool debugText = true;
	public bool noramlizeOutput = true;
	public bool hideMouse = true;
	public enum Types { Horizontal, Vertical, Both }
	public Types type;

	public float xAxis;
	public float yAxis;
	public Vector2 axis
	{
		get { 
			if(simulate && noramlizeOutput)
				return new Vector2(xAxis / Screen.width, yAxis / Screen.height);
			else if (simulate)
				return new Vector2(xAxis , yAxis);
			else
				return new Vector2(xAxis, yAxis); 
		}
	}

	public static Theremin instance;

	void Start()
	{
		if (instance != null)
			throw new Exception("Only one instance of theremin please !");

		instance = this;

		if(!simulate)
		{
			arduino = Arduino.global;
			if (arduino == null)
			{
				print("Fallback to simulate mode");
				simulate = true;
			}
			else
			{
				arduino.Log = (s) => Debug.Log("Arduino: " + s);
				arduino.Setup(ConfigurePins);
				print("Start Arduino" + arduino + " " + arduino.Connected);

				if (!arduino.Connected)
				{
					print("Fallback to simulate mode");
					simulate = true;
				}
			}
			
		}
	}

	void ConfigurePins()
	{
		print("ConfigurePins" + arduino+" " + arduino.Connected);
		simulate = false;
		arduino.pinMode(pin, PinMode.ANALOG);
		arduino.reportAnalog(pin, 1);
	}

	void Update()
	{
		Screen.showCursor = !hideMouse;

		if(simulate)
		{
			Vector2 mouse = Input.mousePosition;
			xAxis = type == Types.Horizontal || type == Types.Both ? Mathf.Clamp(mouse.x, 0, Screen.width) : 0;
			yAxis = type == Types.Vertical || type == Types.Both ? Mathf.Clamp(mouse.y, 0, Screen.height) : 0;
		}
		else if (arduino)
		{
			pinValue = arduino.analogRead(pin);
			xAxis = yAxis = Mathf.Clamp01((Mathf.Log10(pinValue) - 1f ) / 2f);
		}
		if (debugText) print("pinValue " + pinValue + " xAxis " + xAxis);
	}
	void OnGUI()
	{
		if (simulate)
		{
			InitStyles();
			GUI.Box(new Rect(xAxis - 10, Screen.height - yAxis - 10, 20, 20), "", currentStyle);
		}
		if(debugText)
		{
			GUI.Label(new Rect(0,0,100,20), "("+axis.x+","+axis.y+")");
		}
	}

	private void InitStyles()
	{
		if (currentStyle == null)
		{
			currentStyle = new GUIStyle(GUI.skin.box);
			currentStyle.normal.background = MakeTex(2, 2, simulateSquareColor);
		}
	}

	private Texture2D MakeTex(int width, int height, Color col)
	{
		Color[] pix = new Color[width * height];
		for (int i = 0; i < pix.Length; ++i)
		{
			pix[i] = col;
		}
		Texture2D result = new Texture2D(width, height);
		result.SetPixels(pix);
		result.Apply();
		return result;
	}
}
