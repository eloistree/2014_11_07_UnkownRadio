﻿using UnityEngine;
using System.Collections;

public class HexagonPatterns : MonoBehaviour {

	public GameObject prefab;
	float startRadius = 40;
	float stepRadius = 4;
	float stepRadius2 = 10;
	public int[] possibleSegments;
	public bool onlyHexagone;
	public static int segments = 6;

	IEnumerator Start()
	{
		int ex = 0;
		while(true)
		{
			startRadius = 40;
			switch (Random.Range(1, 4))
			{
				case 1 : pattern1(); break;
				case 2 : pattern2(); break;
				case 3 : pattern3(); break;
				case 4 : pattern4(); break;
			}
			ex++;
			if (!onlyHexagone && ex % 3 == 0)
			{
				yield return new WaitForSeconds(9f);
				segments = possibleSegments[Random.Range(0, possibleSegments.Length)];
				
			}
			else
				yield return new WaitForSeconds(5f);
		}
	}


	public void pattern1( )
	{
		int count = Random.Range(7, 15);
		for (int i = 0; i < count; i++)
		{
			startRadius += segments == 4 ? stepRadius * 4 : stepRadius;
			GenerateShape(i, startRadius);
			
		}
	}

	public void pattern2()
	{
		int count = Random.Range(4, 6);
		for (int i = 0; i < count; i++)
		{
			startRadius += segments == 4 ? stepRadius * 4 : stepRadius;
			
			GenerateShape(i, startRadius);
			GenerateShape(i + 3, startRadius, 4);

		}
	}

	public void pattern3()
	{

		int count = Random.Range(4, 6);
		for (int i = 0; i < count; i++)
		{
			startRadius += segments == 4 ? stepRadius2 * 4 : stepRadius2;
			
			GenerateShape(i, startRadius);
			GenerateShape(i + 1, startRadius, 3);
			GenerateShape(i + 2, startRadius, 4);

		}
	}

	public void pattern4()
	{

		int count = Random.Range(2, 5);
		for (int i = 0; i < count; i++)
		{
			startRadius += segments == 4 ? stepRadius2 * 4 : stepRadius2;

			GenerateShape(i, startRadius);
			GenerateShape(i + 1, startRadius);

			GenerateShape(i + 3, startRadius);
			GenerateShape(i + 4, startRadius);

		}
	}

	private HexagoneGenerator GenerateShape(int start, float radius, float size = 2)
	{
		GameObject inst = (GameObject)GameObject.Instantiate(prefab);
		inst.transform.rotation = transform.rotation;
		inst.transform.parent = transform;
		HexagoneGenerator gen = inst.GetComponent<HexagoneGenerator>();
		gen.startSegment = start;
		gen.radius = radius;
		gen.size = size;
		gen.segments = segments;
		return gen;
	}
}
