﻿using UnityEngine;
using System.Collections;

public class HexagonLife : MonoBehaviour {

	public int gameLifes = 5;
	public float timeToWin = 30f;
	public static HexagonLife instance;
	IEnumerator Start () 
	{
		instance = this;

		yield return new WaitForSeconds(timeToWin);

		Time.timeScale = 1;
		if (GameManager.instance!=null)
		GameManager.instance.WinMinigame();
	}
	
	public void Hit () 
	{
		gameLifes--;
		if(gameLifes <= 0)
		{
			Time.timeScale = 1;
			if (GameManager.instance != null)
			GameManager.instance.LooseMinigame();
		}
	}
}
