﻿
using UnityEngine;
using ProceduralToolkit;

	[RequireComponent(typeof (MeshRenderer), typeof (MeshFilter))]
	public class HexagoneGenerator1 : MonoBehaviour {

		public float radius = 10;
		public float size = 10;
        public float height = 10;
		public float segments = 5f;
		public int startSegment = 0;

		public float destroyRadius = 1f;
		public float decreaseSpeed = 0;
		public int restPieces;
        private MeshDraft draft;
        private Gradient gradient;
		private MeshFilter filter;
		public bool removable;
        private void Start()
        {
			filter = GetComponent<MeshFilter>();
        }

		Mesh m;
        private void Update()
        {
			draft = MeshE.Prism2Draft1(radius, radius - size, segments, startSegment,  height, restPieces);
			
			if (draft != null)
			{
				Destroy(m);
				m = null;
				Destroy(filter.mesh);
				m = draft.ToMesh();
				

				filter.mesh = m;
				if (collider != null && collider is MeshCollider)
					(collider as MeshCollider).mesh = m;
				if (renderer.enabled == false)
					renderer.enabled = true;
			}
			else renderer.enabled = false;

			radius -= Time.deltaTime * decreaseSpeed * .01f; 
			if (removable && radius < destroyRadius)
				Destroy(gameObject);
        }

      
   
}