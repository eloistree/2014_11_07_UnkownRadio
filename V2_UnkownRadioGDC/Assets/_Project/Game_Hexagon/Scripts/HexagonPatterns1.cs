﻿using UnityEngine;
using System.Collections;

public class HexagonPatterns1 : MonoBehaviour {

	private float currentMaxRadius = 0;
	public float minRadiusToSpawn = 10; 
	public GameObject[] patterns;
	private int index;
	public float checkTime = 5f;

	IEnumerator Start()
	{
		
		while(true)
		{
			CheckMaxDistance();
			if (currentMaxRadius < minRadiusToSpawn)
			{
				GameObject inst = (GameObject)Instantiate(patterns[index]);
				inst.transform.rotation = transform.rotation;
				inst.transform.parent = transform;
			}
			yield return new WaitForSeconds(checkTime);
			index = index.IncrementOrResetIfHigherThan(patterns.Length - 1);
		}
	}
	void CheckMaxDistance()
	{
		currentMaxRadius = 0;
		GameObject[] allBricks = GameObject.FindGameObjectsWithTag("Bricks");
		foreach (GameObject go in allBricks)
		{
			HexagoneGenerator1 hg = go.GetComponent<HexagoneGenerator1>();
			if (hg == null) continue;
			float lng = hg.radius;
			if (lng > currentMaxRadius)
				currentMaxRadius = lng;
		}		
		Debug.Log("CheckMaxDistance "+currentMaxRadius);
	}
}
