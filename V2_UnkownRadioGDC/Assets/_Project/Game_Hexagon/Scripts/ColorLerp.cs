﻿using UnityEngine;
using System.Collections;

public class ColorLerp : MonoBehaviour {

	public GameObject hexagonPrefab;
	public GameObject backgroundPrefab;

	public Color[] colors;
	public Color[] backgroundColors;

	private int index;

	void Start () 
	{
		Next();
	}

	void Next()
	{

		iTween.ValueTo(gameObject, iTween.Hash("oncomplete", "Next", "onupdate", "OnColorUpdated", "from", hexagonPrefab.renderer.sharedMaterial.color, "to", colors[index], "time", 5f));
		iTween.ValueTo(gameObject, iTween.Hash("onupdate", "OnBackgroundColorUpdated", "from", backgroundPrefab.renderer.sharedMaterial.color, "to", backgroundColors[index], "time", 5f));
		index++;
		if (index > colors.Length - 1)
			index = 0;
	}
	
	// Update is called once per frame
	void OnBackgroundColorUpdated(Color c)
	{
		backgroundPrefab.renderer.sharedMaterial.color = c;
	}
	void OnColorUpdated(Color c)
	{
		hexagonPrefab.renderer.sharedMaterial.color = c;
	}
}
