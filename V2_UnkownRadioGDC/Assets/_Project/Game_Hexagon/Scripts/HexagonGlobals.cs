﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class HexagonGlobals : MonoBehaviour {

	public static float segments = 6;
	public static float height = 2;
	public static float speed = 10f;
	public static HexagonGlobals instance;

	public GameObject prefab;
	public float modeDuration = 5f;
	public float tweenDuration = 1f;
	public float startHeight = 2;
	public float startSpeed = 10;
	public float speedIncrement = 0.1f;
	public int[] modeList;
	private int modeIndex;
	void Awake()
	{ 
		instance = this; 
	}
	IEnumerator Start()
	{
		
		segments = modeList[0];
		height = startHeight;
		speed = startSpeed;

		while(true)
		{
			DOTween.To(() => segments, x => segments = x, modeList[modeIndex], tweenDuration);
			yield return new WaitForSeconds(modeDuration);
			modeIndex = modeIndex.IncrementOrResetIfHigherThan(modeList.Length - 1);
		}
	}

	public static HexagoneGenerator1 GenerateShape(Transform tr, Material mat, int start, float radius, float size = 2, float segments = 4, float height = 2f, bool removable = false, float speed = 0)
	{
		GameObject inst = (GameObject)GameObject.Instantiate(instance.prefab);
		inst.tag = tr.gameObject.tag;
		inst.transform.position = tr.position;
		inst.transform.rotation = tr.rotation;
		inst.transform.parent = tr;
		MeshRenderer mr = inst.GetComponent<MeshRenderer>();
		mr.sharedMaterial = mat;
		HexagoneGenerator1 gen = inst.GetComponent<HexagoneGenerator1>();
		gen.startSegment = start;
		gen.decreaseSpeed = speed;
		gen.radius = radius;
		gen.height = height;
		gen.size = size;
		gen.segments = segments;
		gen.removable = removable;
		return gen;
	}
}
