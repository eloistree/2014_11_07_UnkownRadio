﻿using UnityEngine;
using System.Collections;

public class Mouvement : MonoBehaviour {

	public float ratioF;
	private float pasty=-1.0f;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if(pasty < 0.5f)
			pasty=Theremin.instance.axis.x;
		float dy=(Theremin.instance.axis.x-pasty)/Time.deltaTime;
		pasty=Theremin.instance.axis.x;
		dy*=-1;
		if(dy < 0) dy *=-1.0f;
        float forceup = ratioF * dy * Time.deltaTime;
		rigidbody.AddForce(0f,forceup,0f,ForceMode.Force);
		Debug.DrawLine(rigidbody.position,forceup*Vector3.up,Color.black,0f,true);
	}
}
