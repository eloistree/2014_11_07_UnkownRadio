﻿using UnityEngine;
using System.Collections;

public class putsomeblock : MonoBehaviour {


	public float poptime=5f; 
	private float lasttime=-10f;
	public Object prefabobstacle;
	private float lastrand=0.0f;
	private float K=1f;
	private float h=4f;
	private float scaley;
	public float speedblock;
	public float space;
	// Use this for initialization
	void Start () {
		scaley = ((GameObject) prefabobstacle).transform.localScale.y;
		speedblock = space/poptime;

	}
	
	// Update is called once per frame
	void Update () {
		if(Time.timeSinceLevelLoad - lasttime > poptime && !gamemanager.singleton.gameover){
			K=Mathf.Min(8f,K+0.5f);
			h=Mathf.Max(2f,h-0.025f);

			lastrand= Random.Range(lastrand-K,lastrand+K);

			lastrand=Mathf.Clamp(lastrand,-5f+h/2f,5f-h/2f);
			GameObject murtop = (GameObject) Object.Instantiate(prefabobstacle,new Vector3(50f,-(scaley/2f+lastrand+h/2f),49.5f),Quaternion.identity);
			GameObject murbot = (GameObject) Object.Instantiate(prefabobstacle,new Vector3(50f,scaley/2f-lastrand+h/2f,49.5f),Quaternion.identity);
			murtop.GetComponent<mouvingblock>().speed=speedblock;
			murbot.GetComponent<mouvingblock>().speed=speedblock;

			poptime = (poptime-2f)*0.85f+2f;

			speedblock = space/poptime;


			lasttime=Time.timeSinceLevelLoad;
			murbot.GetComponent<mouvingblock>().generatecrate(space);
		}
	}
}
