﻿using UnityEngine;
using System.Collections;

public class UpDown : MonoBehaviour {
	
	private float speed;
	private bool goingtop;
	// Use this for initialization
	void Start () {
		speed = Random.Range(0.1f,0.3f);
		goingtop = Random.Range(0,1)==1;
	}
	
	// Update is called once per frame
	void Update () {
		if(goingtop){
			if(this.transform.position.y>2){
				goingtop= ! goingtop;
				this.transform.Translate(Vector3.down*speed*Time.deltaTime);
			}else{
                this.transform.Translate(Vector3.up * speed * Time.deltaTime);
			}
		}else{
			if(this.transform.position.y<-2){
				goingtop= ! goingtop;
                this.transform.Translate(Vector3.up * speed * Time.deltaTime);
			}else{
                this.transform.Translate(Vector3.down * speed * Time.deltaTime);
			}
		}
	}
}
