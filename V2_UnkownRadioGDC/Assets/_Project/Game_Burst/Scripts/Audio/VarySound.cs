﻿using UnityEngine;
using System.Collections;

namespace ARM {

	public class VarySound : MonoBehaviour {

		public bool PlayOnEnabled = true;
		public float MaxPitch = 1.25f;
		public float MinPitch = 0.75f;
		public float MaxVolume = 1.0f;
		public float MinVolume = 0.75f;

		void Start () {
			Play();
		}

		void OnEnable()
		{
			Play();
		}

		public void Play()
		{
			audio.pitch = Random.Range(MinPitch, MaxPitch);
			audio.volume = Random.Range(MinVolume, MaxVolume);
			audio.Play();
		}

	}
}