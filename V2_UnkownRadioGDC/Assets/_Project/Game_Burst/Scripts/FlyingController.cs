﻿using UnityEngine;
using System.Collections;
using ARM;

public class FlyingController : MonoBehaviour {

    public Transform StartingPoint;
    public Transform PoolContainer;

    private float ZSpeed = 5f;
    private float MaxZSpeed = 100f;
    private float rotationSpeed = 360f;

    [HideInInspector]
    public bool isAlive = true;
    
    private SENaturalBloomAndDirtyLens SEN;
    private float oldSENIntensity;

    void Awake()
    {
        camera.transform.position = StartingPoint.position;
        camera.transform.rotation = StartingPoint.rotation;
        SEN = GetComponent<SENaturalBloomAndDirtyLens>();
        oldSENIntensity = SEN.bloomIntensity;
    }
	
    void Start()
    {
        isAlive = true;
        SEN.bloomIntensity = oldSENIntensity;
    }

    void Update () {
        if (isAlive)
        {
            float Zspeed = (rigidbody.velocity.z > MaxZSpeed) ? 0 : ZSpeed * Time.deltaTime;
            PoolContainer.Rotate(new Vector3(0, 0, (Theremin.instance.axis.x - 0.5f) * rotationSpeed * Time.deltaTime));
            rigidbody.AddForce(0, 0, Zspeed, ForceMode.VelocityChange);
        }
        else
        {
            SEN.bloomIntensity = Mathf.Lerp(SEN.bloomIntensity,0.4f, Time.deltaTime * 2f);
        }
	}

    void OnCollisionEnter(Collision c)
    {
        if(c.gameObject.layer == LayerMask.NameToLayer("Killing"))
        {
            MultiplePooled.GetPooledObjectByName("Explosions", transform);
            rigidbody.velocity /= 2;
            rigidbody.AddForce(new Vector3(Random.Range(-2f, 2f), Random.Range(0f, 2f), Random.Range(-5f, 0f)), ForceMode.Impulse);
            GameObject.Destroy(c.gameObject);
            isAlive = false;
            Invoke("GameOver", 5.0f);
        }
    }

    public void GameOver()
    {
        GameManager.instance.LooseMinigame();
    }
}
