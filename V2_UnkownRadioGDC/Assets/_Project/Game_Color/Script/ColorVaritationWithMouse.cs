﻿using UnityEngine;
using System.Collections;
using Uniduino;

namespace unknowradio.colorgame
{
    public class ColorVaritationWithMouse : MonoBehaviour
    {

    
        public SpriteColorManager spriteColor;
        
        public float ratioColor = 0.7f;

        public Color horizontalColor;
        public Color verticalColor;

        public float pourcentHorizontal;
        public float pourcentVertical;

        public float rationMouseMove = 1f;
        public float x, y;
        void Update()
        {

			bool hasChange = x != Theremin.instance.axis.x || y != Theremin.instance.axis.y;
            if (!hasChange) return;
            if (Arduino.global && Arduino.global.Connected)
            {
                x = Theremin.instance.axis.x * ratioColor;
                y = Theremin.instance.axis.y * ratioColor;
            }
            else {

                x = Theremin.instance.axis.x ;
                y = Theremin.instance.axis.y ;
            }
            pourcentHorizontal = Mathf.Clamp(x, 0f, 1f);
            pourcentVertical = Mathf.Clamp(y, 0f, 1f);
            

            spriteColor.ChangeColor(GetMixedColor(), true);





        }

        private Color GetMixedColor()
        {
            // Color average = new Color(0, 0, 0, 1f);
            //horizontalColor = GetSussessifRgbFor(pourcentHorizontal);
            //verticalColor = GetSussessifRgbFor(pourcentVertical);
            horizontalColor = ColorsGetter.GetLinkedColor(pourcentHorizontal, GameColorManaged.Colors);
         
            return horizontalColor;
            //verticalColor = GetSussessifRgbFor(pourcentVertical);

            //average.r = (horizontalColor.r + verticalColor.r) / 2f;
            //average.g = (horizontalColor.g + verticalColor.g) / 2f;
            //average.b = (horizontalColor.b + verticalColor.b) / 2f;


            //return average;
        }
    }
}