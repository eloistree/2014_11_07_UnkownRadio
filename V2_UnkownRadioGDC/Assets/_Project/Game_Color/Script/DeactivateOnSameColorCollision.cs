﻿using UnityEngine;
using System.Collections;
namespace unknowradio.colorgame
{
[RequireComponent(typeof(SpriteColorManager))]
public class DeactivateOnSameColorCollision : MonoBehaviour {

    public SpriteColorManager spriteColor;
    [Range(0f,1f)]
    public float pourcentRange = 0.1f;
    public bool withAlphaCheck=true;
    public bool delete;
    public bool deactivate=true;
    public void Start() 
    {

        if (!spriteColor) spriteColor = GetComponent<SpriteColorManager>() as SpriteColorManager;
    }

    public void OnCollisionStay2D(Collision2D col) 
    {
        SpriteColorManager colSpriteColor =col.gameObject.GetComponent<SpriteColorManager>() as SpriteColorManager;
        if (colSpriteColor) 
        {
            bool isSameColor = true;
            if (isSameColor && !IsACloseEnoughtOfB(colSpriteColor.currentColor.r, spriteColor.currentColor.r))
            { isSameColor = false; }

            if (isSameColor && !IsACloseEnoughtOfB(colSpriteColor.currentColor.g, spriteColor.currentColor.g))
            { isSameColor = false; }

            if (isSameColor && !IsACloseEnoughtOfB(colSpriteColor.currentColor.b, spriteColor.currentColor.b))
            { isSameColor = false; }

            if (isSameColor)
            {
                if (delete) Destroy(this.gameObject);
                if (deactivate) gameObject.SetActive(false);
            }
        }
    }

    private bool IsACloseEnoughtOfB(float a, float b)
    {
        return a > (b - pourcentRange) && a < (b + pourcentRange);
    }
}
}