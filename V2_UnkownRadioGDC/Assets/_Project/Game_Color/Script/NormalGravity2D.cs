﻿using UnityEngine;
using System.Collections;
namespace unknowradio.colorgame
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class NormalGravity2D : MonoBehaviour
    {

        public Checker hasGround;
        public float decelerationWhenGoUp = 9.81f;
        public float decelerationWhenGoDown = 9.81f;

        void Start()
        {
            if (!hasGround) { Destroy(this); return; }

        }

        void Update()
        {
            if (!hasGround) { return; }
            if (hasGround.IsColliding2D())
            {
                rigidbody2D.gravityScale = 1f;
                return;
            }
            if (rigidbody2D.velocity.y > 0f)
            {
                rigidbody2D.gravityScale += decelerationWhenGoUp * Time.deltaTime;
            }
            else rigidbody2D.gravityScale += decelerationWhenGoDown * Time.deltaTime;


        }
    }
}