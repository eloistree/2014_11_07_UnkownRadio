﻿using UnityEngine;
using System.Collections;

namespace unknowradio.colorgame
{
    public class ChangeColorRegulary : MonoBehaviour
    {


        public SpriteColorManager spriteColor;
        public float randomFrom = 1f;
        public float randomTo = 3f;
        private float countdown;


        void Start() 
        {
            if (!spriteColor) spriteColor=GetComponent<SpriteColorManager>() as SpriteColorManager;
        }
        void Update()
        {
            countdown -= Time.deltaTime;
            if (countdown <= 0) 
            {
                countdown = Random.Range(randomFrom, randomTo);
                ChangeColor();
            }
        }

        private void ChangeColor()
        {
            if (spriteColor)
            {
                Color c = new Color();
                c = ColorsGetter.GetLinkedColor(Random.Range(0f, 1f),GameColorManaged.Colors);

                spriteColor.ChangeColor(c, false);
            }
        }
    }
}