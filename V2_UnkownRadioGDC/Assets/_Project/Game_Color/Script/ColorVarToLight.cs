﻿using UnityEngine;
using System.Collections;
namespace unknowradio.colorgame
{
[RequireComponent(typeof(Light))]
public class ColorVarToLight : MonoBehaviour {

    public SpriteColorManager objectColor;
    public Light linkedLight;

    void Start() 
    {
        if (linkedLight)
            linkedLight = GetComponent<Light>() as Light;
        if (objectColor)
            objectColor = GetComponent<SpriteColorManager>() as SpriteColorManager;
    }

	void Update () {
        if(linkedLight)
        linkedLight.color = objectColor.currentColor; 
	
	}
}
}