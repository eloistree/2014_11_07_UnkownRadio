﻿using UnityEngine;
using System.Collections;

public class InterludeController : MonoBehaviour {

    public float delay = 3f;

	void Start () {
        if (GameManager.instance.Life > 0)
        {
            Invoke("LoadNextMiniGame", delay);
        }
        else
        {
            Invoke("SwitchToMenu", delay);
        }
	}
	
    public void LoadNextMiniGame()
    {
        GameManager.instance.LoadMiniGame();
    }

    public void SwitchToMenu()
    {
        GameManager.instance.SwitchToMenu();
    }
}
