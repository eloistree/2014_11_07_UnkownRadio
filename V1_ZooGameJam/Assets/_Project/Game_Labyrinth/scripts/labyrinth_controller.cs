﻿using UnityEngine;
using System.Collections;

public class labyrinth_controller : MonoBehaviour {
	public float strength = 30.0f;

	private Vector2 corrector = new Vector2(0.5f, 0.5f);
	private bool isActive = false;
	private bool alternativeController;

	public GameObject mainMesh;

	private bool useRandomizedController = false;
	private bool useOnlyOneAxis = true;

    private float currentAngle = 0.0f;

	private Vector3 originalPosition;

	// Use this for initialization
	void Start () {
		alternativeController = (Random.Range(0, 10) % 2 == 0);
		DeactivateController();
		Invoke("ActivateController", 1.0f);
		originalPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if(isActive) {
			if(useOnlyOneAxis) {
				transform.eulerAngles = Vector3.zero;
				transform.position = originalPosition;

				float tx = Theremin.instance.axis.x - 0.25f;
				if(tx < 0.0f) tx = 0.0f;
				tx /= 0.75f;

				float a = tx * 2.0f * Mathf.PI;

                currentAngle += (a - currentAngle) * Time.deltaTime * 2.0f;

                float x = strength * Mathf.Sin(currentAngle);
                float y = strength * Mathf.Cos(currentAngle);

				transform.RotateAround(mainMesh.renderer.bounds.center, Vector3.right, x);
				transform.RotateAround(mainMesh.renderer.bounds.center, Vector3.forward, y);
			} else {
				Vector2 axis;

				if(useRandomizedController && alternativeController) {
					axis.x = Theremin.instance.axis.y;
					axis.y = Theremin.instance.axis.x;
				} else {
					axis = Theremin.instance.axis;
				}

				axis -= corrector;
				transform.eulerAngles = new Vector3(
					strength * axis.y,
					0.0f,
					strength * axis.x * -1.0f
					);
			}
		}
	}

	public void ActivateController() {
		isActive = true;
	}

	public void DeactivateController() {
		isActive = false;
	}
}
