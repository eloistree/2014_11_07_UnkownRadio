﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
class GameData 
{
    public double bestScore;
    public int gamesPlayed;
}

public class GameManager : MonoBehaviour {

    const string _menu = "menu";
    const string _interlude = "Interlude";
    const string dataFileName = "GameStats.dat";
    const int maxLife = 12;

    public string[] scenes;         // List of the scenes (add them in the editor)

    [HideInInspector, System.NonSerialized]
    private double bestScore;        // The best score ever

    [HideInInspector]
    private double currentScore;     // The current score of the playerLe score (en cours) du joueur

    public double CurrentScore
    {
        get { return currentScore; }
        private set { currentScore = value; }
    }

    [HideInInspector, System.NonSerialized]
    public int gamesPlayed;         // The number of games played since the beginning

    [Range(0, maxLife), System.NonSerialized]
    private int life = maxLife;      // The number of life for our player

    public int Life
    {
        get { return life; }
        private set { life = value; }
    }

    private String nextMiniGame;  // The next Minigame loaded asynchronely

    private AsyncOperation asyncInterlude; // The Async Operator of the Interlude (TVNoise)

    private List<String> minigames;   // The Random unique list of Minigames

    private static GameManager _instance;

    public static GameManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();

                if(_instance)
                    DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
            InitializeMiniGames();
        }
        else
        {
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }

    void Update()
    {
        if (Application.loadedLevelName != _menu)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                SwitchToMenu();
        }
    }

    private void InitializeMiniGames()
    {
        minigames = new List<String>();

        // First I shuffle the available scenes
        minigames = Toolbox.RandomizeStrings(scenes).ToList();

        // Finaly I re-shuffle that list
        ShuffleMiniGames();
    }

    private void ShuffleMiniGames()
    {
        minigames.RandomizeList();
    }

    private void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/" + dataFileName, FileMode.OpenOrCreate);

        GameData data = new GameData();
        data.bestScore = bestScore;
        data.gamesPlayed = gamesPlayed;

        bf.Serialize(file, data);
        file.Close();
    }


    private void Load()
    {
        string persistentDataPath = Application.persistentDataPath + "/" + dataFileName;

        if(File.Exists(persistentDataPath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(persistentDataPath, FileMode.Open);
            GameData data = bf.Deserialize(file) as GameData;

            bestScore = data.bestScore;
            gamesPlayed = data.gamesPlayed;
        }
    }

    public void SetNextMiniGame()
    {
        if (string.IsNullOrEmpty(nextMiniGame))
        {
            nextMiniGame = minigames.ElementAt(0);
            return;
        }

        for(int i = 0; i < minigames.Count; i++)
        {
            if (minigames.ElementAt(i) == nextMiniGame)
            {
                // Verify if we reach the end of a cycle of Mini games.
                if(i+1 < minigames.Count)
                {
                    nextMiniGame = minigames.ElementAt(i + 1);
                    return;
                }
                else
                {
                    // End of cycle detected !
                    // Shuffle the Mini Games list and avoid having the same as next one.
                    do
                        ShuffleMiniGames();
                    while (minigames.ElementAt(0) == nextMiniGame);
                    nextMiniGame = minigames.ElementAt(0);
                    return;
                }
            }
        }
    }

    public void LoadMiniGame()
    {
        SetNextMiniGame();
        if (!string.IsNullOrEmpty(nextMiniGame))
        {
            gamesPlayed++;
            Application.LoadLevel(nextMiniGame);
        }
        else
        {
            Debug.LogError("Error !/nNext Mini Game cannot be find.");
            SwitchToMenu();
        }
    }

    /// <summary>
    /// Call this when Player Loose the Mini Game.
    /// </summary>
    public void LooseMinigame()
    {
        life--;
        DisplayInterlude();
    }

    public void WinMinigame()
    {
        currentScore++;
        if (currentScore > bestScore) bestScore = currentScore;
        DisplayInterlude();
    }

    /// <summary>
    /// Save the stats and Display the Interlude (TVSnow).
    /// </summary>
    public void DisplayInterlude()
    {
        Save();
        // Load the next scene via the Interlude
        Application.LoadLevel(_interlude);
    }

    public void SwitchToMenu()
    {
        life = maxLife;
        ShuffleMiniGames();
        Application.LoadLevel(_menu);
    }

}
