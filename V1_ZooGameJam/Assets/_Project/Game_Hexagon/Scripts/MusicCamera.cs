﻿using UnityEngine;
using System.Collections;

public class MusicCamera : MonoBehaviour {

	private Vector3 target;
	public float duration = 1;
	public float forwardFactor = .2f;
	public AudioClip[] clips;

	void Start () 
	{
		target = transform.position + transform.forward* forwardFactor;
		audio.clip = clips[Random.Range(0, clips.Length)];
		audio.Play();

		iTween.MoveTo(gameObject, iTween.Hash("position", target, "easetype", iTween.EaseType.easeOutBack, "time", duration, "looptype", iTween.LoopType.pingPong));
	}
	
	// Update is called once per frame
	void Update ()
	{
		audio.pitch = Time.timeScale;
	}
}
