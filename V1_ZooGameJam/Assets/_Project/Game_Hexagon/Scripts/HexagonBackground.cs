﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HexagonBackground : MonoBehaviour {

	public GameObject prefab;
	private List<GameObject> insts;
	
	void Start()
	{
		insts = new List<GameObject>();
	}
	void Update ()
	{
		//print("HexagonPatterns.segments "+HexagonPatterns.segments);
		int count = HexagonPatterns.segments;
		int ex = 0;
		for( int i = 0; i < count; i+=2)
		{
			GenerateShape(ex++, i, 100, 100);
		}
		for (int i = ex; i < insts.Count; i ++)
		{
			//insts[i].GetComponent<MeshFilter>().mesh = null;
			GameObject go = insts[i];
			insts.Remove(go); 
			Destroy(go);
			

		}
	}

	private HexagoneGenerator GenerateShape(int index, int start, float radius, float size = 2)
	{
		GameObject inst;
		if (insts.Count < index + 1)
		{
			inst = (GameObject)Instantiate(prefab);
			insts.Add(inst);
		}
		else
			inst = insts[index];
		inst.transform.rotation = transform.rotation;
		inst.transform.parent = transform;
		HexagoneGenerator gen = inst.GetComponent<HexagoneGenerator>();
		gen.startSegment = start;
		gen.radius = radius;
		gen.height = 0.1f;
		gen.segments = HexagonPatterns.segments;
		gen.decreaseSpeed = 0;
		gen.size = size;
		return gen;
	}
}
