﻿using UnityEngine;
using System.Collections;

public class TriangleTopTrigger : MonoBehaviour {

	public AudioClip hitSound;
    public TrianglePlayer triangle;

	void Start () {
	
	}
	
	// Update is called once per frame
	void OnTriggerEnter ( Collider c ) 
	{
		//print("OnTriggerEnter top " + c.gameObject.name);
		audio.clip = hitSound;
		audio.Play();
		HexagonLife.instance.Hit();
        triangle.Hit();
		Destroy(c);
	}
}
