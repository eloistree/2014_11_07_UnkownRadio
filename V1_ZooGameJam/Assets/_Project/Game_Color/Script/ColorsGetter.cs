﻿using UnityEngine;
using System.Collections;
namespace unknowradio.colorgame
{
    public class ColorsGetter
    {


        public static Color GetSussessifRgbFor(float pourcent)
        {
            Color c = new Color(0f, 0, 0, 1f);

            if (pourcent <= 0f)
            { c.r = 1f; }
            else if (pourcent < 1f / 3f)
            {
                pourcent = Mathf.Clamp(pourcent * 3f, 0f, 1f);
                c.r = pourcent;
            }
            else if (pourcent < 2f / 3f)
            {
                pourcent -= 1f / 3f;
                pourcent = Mathf.Clamp(pourcent * 3f, 0f, 1f);
                c.g = pourcent;
            }
            else
            {
                pourcent -= 2f / 3f;
                pourcent = Mathf.Clamp(pourcent * 3f, 0f, 1f);
                c.b = pourcent;
            }

            return c;
        }

        public static Color GetAllColorFor(float pourcent)
        {
            Color c = new Color(0, 0, 0, 1f);
            if (pourcent <= 0f)
            { c.r = 1f; }
            //red to purple
            else if (pourcent < 1f / 7f)
            {
                pourcent = Mathf.Clamp(pourcent * 7f, 0f, 1f);
                c.r = 1f;
                c.b = pourcent;

            }
            //purple to blue
            else if (pourcent < 2f / 7f)
            {
                pourcent -= 1f / 7f;
                pourcent = Mathf.Clamp(pourcent * 7f, 0f, 1f);
                c.b = 1f;
                c.r = 1f - pourcent;
            }
            //blue to skyblue
            else if (pourcent < 3f / 7f)
            {
                pourcent -= 2f / 7f;
                pourcent = Mathf.Clamp(pourcent * 7f, 0f, 1f);
                c.b = 1f;
                c.g = pourcent;
            }
            // skyblue to green
            else if (pourcent < 4f / 7f)
            {
                pourcent -= 3f / 7f;
                pourcent = Mathf.Clamp(pourcent * 7f, 0f, 1f);
                c.g = 1f;
                c.b = 1f - pourcent;
            }
            //green to yellow
            else if (pourcent < 5f / 7f)
            {
                pourcent -= 4f / 7f;
                pourcent = Mathf.Clamp(pourcent * 7f, 0f, 1f);
                c.g = 1f;
                c.r = pourcent;
            }
            //yellow to red
            else if (pourcent < 6f / 7f)
            {
                pourcent -= 5f / 7f;
                pourcent = Mathf.Clamp(pourcent * 7f, 0f, 1f);
                c.r = 1f;
                c.g = 1f - pourcent;
            }
            else c.r = 1f;
            return c;
        }
    }
}