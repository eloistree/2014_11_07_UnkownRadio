﻿using UnityEngine;
using System.Collections;

public class ColorLevelState  {


    public delegate void OnLevelWin(string sceneName, bool win, float timeToWin);
    public static OnLevelWin onLevelWin;

    public static void EndOfLevelDetected(bool win,string sceneToLoad) 
    {
        if (onLevelWin != null) onLevelWin(sceneToLoad, win, Time.timeSinceLevelLoad);

        //??? FUTURE CODE
        Player.Life -= 1;

    
    }


}
