﻿using UnityEngine;
using System.Collections;

public class MySpectrum : MonoBehaviour {

    public enum Frequence : int { _64 = 64, _128 = 128, _256 = 256, _512 = 512, _1024 = 1024, _2048 = 2048, _4096 = 4096, _8192 = 8192 };
    public Frequence frequence;
    public AudioListener listener;
    private float[] freqData;
    public float [] band;
    public GameObject [] g;
    
    
    public int GetIndexOf(float pourcent, ref float [] values, bool mathRound=true )
    {
        float tmp;
        return GetIndexOf(pourcent, ref values, out tmp, mathRound);
    }
    public int GetIndexOf(float pourcent, ref float [] values, out float pourcentLeft, bool mathRound=true )
    {
        pourcentLeft = 0f;
        if (values == null) return 0;
        int indexNbr = values.Length-1;

        if (indexNbr < 2) return 0;

        pourcent = Mathf.Clamp(pourcent, 0f, 1f);
      



        float valueByUnit = ((float)indexNbr) / 100f;
        float where =  valueByUnit * pourcent;

        pourcentLeft = where % valueByUnit;

        if (mathRound) return Mathf.RoundToInt(where * 100f);
        else return (int)(where * 100f);
    
    
    }
    public float GetValueAt(float pourcent, ref float[] values)
    {
        int iMin, iMax;
        return GetValueAt(pourcent, ref values, out iMin, out iMax);
    }
    public float GetValueAt(float pourcent, ref float[] values, out int iMin, out int iMax)
    {

        float pourcentLeft;
        int lenght = values.Length;
         iMin = GetIndexOf(pourcent, ref values, out pourcentLeft, false);
         iMax = iMin + 1;

        iMax = iMax>lenght-1?lenght-1:iMax;
        iMin = iMin>lenght-2?lenght-2:iMin;

        float result = values[iMin] + (values[iMax] - values[iMin]) * pourcentLeft;
        return result;
    }


    

    public float GetAverage() {
        float average=0f;
        for (int i = 0; i < freqData.Length; i++) 
        {
            if (i == 0) average = freqData[0];
            else
                average = (average+ freqData[i])/2f;
            
        }
        return average;
    }

    private Frequence lastFrequence;
     
    void Start()    
    {

        lastFrequence = frequence;
        InitData();
         InvokeRepeating("check", 0, 1.0f/15.0f);
    }

    private void InitData()
    {
        freqData = new float[(int)frequence];

        int n = freqData.Length;
        int k = 0;
        for (var j = 0; j < freqData.Length; j++)
        {
            n /= 2;
            if (n <= 0) break;
            k++;
        }
        band = new float[k + 1];

        if (g != null && g.Length > 0) 
        {
            foreach (GameObject gamo in g)
                Destroy(gamo);
        }
        
        g = new GameObject[k + 1];
        for (var i = 0; i < band.Length; i++)
        {
            band[i] = 0;
            g[i] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            g[i].transform.position = new Vector3(i, 0, 0);
        }


    }
     
void check()
{

    
    if (listener == null || listener.audio == null) return;
    if (lastFrequence != frequence)
    {
        lastFrequence = frequence;
        InitData();
    }
    listener.audio.GetSpectrumData (freqData, 0, FFTWindow.Rectangular);
           
    int k = 0;
    int crossover = 2; 
    for(int i =0;i< freqData.Length;i++)
    {  
        var d = freqData[i];
        var b = band[k];       
        band[k] = (d>b)? d:b;   // find the max as the peak value in that frequency band.
        if (i>crossover-3)
        {
                k++;
                crossover *= 2;   // frequency crossover point for each band.
            Vector3 v = g[k].transform.position ;
            v.y=band[k]*32;
            g[k].transform.position = v;
                band[k] = 0;
        }
    }
    //int iMin,iMax;
    //float result = GetValueAt(pct, ref band, out iMin,out iMax);
    //print(">>Index: " + iMin);
    //print(">>Result: " + result);
    //print(">>A>: " + band[iMin]);
    //print(">>B>: " + band[iMax]);

}


//    [Range(0f,1f)]
//public float pct;

}
