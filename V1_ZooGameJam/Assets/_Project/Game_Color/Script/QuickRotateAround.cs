﻿using UnityEngine;
using System.Collections;

public class QuickRotateAround : MonoBehaviour {

    public Transform aroundWho;
    public float angle=10;
	// Use this for initialization
	void Start () {
        if (aroundWho) Destroy(this);
	}
	
	// Update is called once per frame
	void Update () {

        transform.RotateAround(aroundWho.position, aroundWho.up, angle*Time.deltaTime);
	
	}
}
