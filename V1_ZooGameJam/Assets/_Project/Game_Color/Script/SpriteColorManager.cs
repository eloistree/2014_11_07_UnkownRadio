﻿using UnityEngine;
using System.Collections;
namespace unknowradio.colorgame
{
    public class SpriteColorManager : MonoBehaviour
    {

        public SpriteRenderer spriteRender;


        public float timeBeforeStargeChanging = 0f;
        public float timeToChange = 3f;
        public float countDown = 0f;
        public float startCountDown = 0f;
        public Color currentColor;
        public Color fromColor;
        public Color nextColor;
        public bool changeImmediatAtStart = true;

        public bool withFixeAlpha;
        public float fixedAlpha = 255f;



        void Start()
        {

            ChangeColor(currentColor, true);
        }

        void Update()
        {

            if (startCountDown > 0)
            {
                startCountDown -= Time.deltaTime;
                return;
            }

            if (countDown <= 0) return;
            if (nextColor == currentColor) return;
            countDown -= Time.deltaTime;

            currentColor = Color.Lerp(fromColor, nextColor, Mathf.Clamp((timeToChange - countDown) / timeToChange, 0f, 1f));


            spriteRender.color = currentColor;
            if (countDown <= 0)
            {
                ChangeColor(nextColor, true);
            }
        }

        public void ChangeColor(Color next, bool immediately)
        {
            if (withFixeAlpha)
                next.a = fixedAlpha;
            nextColor = next;
            if (!immediately)
            {
                countDown = timeToChange;
                startCountDown = timeBeforeStargeChanging;
            }
            else
            {

                currentColor = nextColor;
                fromColor = nextColor;
                countDown = 0;

                spriteRender.color = currentColor;
            }
        }

        public void SetAlpha(float pourcent)
        {
            fixedAlpha = Mathf.Clamp(pourcent, 0f, 1f);
        }
    }
}
