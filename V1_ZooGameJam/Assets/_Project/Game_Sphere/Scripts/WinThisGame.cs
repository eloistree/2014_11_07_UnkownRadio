﻿using UnityEngine;
using System.Collections;

public class WinThisGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if (other.gameObject.name == "First Person Controller")
        {
            print("Game Over");
            GameObject.FindGameObjectWithTag("GameController").GetComponent<SS_GameLogical>().Win();
        }        
    }
}
