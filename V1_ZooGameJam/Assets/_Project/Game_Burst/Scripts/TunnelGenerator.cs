﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace ARM
{
    public class TunnelGenerator : MonoBehaviour
    {

        public Transform structureGroup;
        public AudioClip VictorySound;
        public Text SectionLeftLabel;

        private float sectionZLength = 32;
        private int startingSections = 16;
        private int section_ID = 0;
        private int cycle = 2;
        private bool endReached = false;

        private FlyingController FC;

        public readonly string[] map = { "WT", "BT", "WT", "BT", "WT", "BT", "WT", "BT", "WT", "BT", "WT", "BT", "WT", "B", "W", "F2", "E", "WT", "E", "BT", "F2", "WT", "BT", "K6", "W", "H", "B", "WT", "BT", "K1", "BT", "WT", "F2", "E", "K1", "B", "K5", "B", "K6", "W", "B", "W", "K1", "B", "W", "B", "W", "E", "F", "E", "K3", "F2", "BT", "BT", "BT", "E", "K2", "B", "F", "W", "K7", "WT", "BT", "E", "W", "WT","K8", "H","WT","BT","K8", "E","F","F2","B","BT","W","WT","H","K4","E","BT","BT","BT","F2","H","E", "B","K2","WT","BT","K1","B","W","K4","B","BT","W","WT","K7","F2","BT", "F", "K5","BT","F2","F","E","BT","BT","BT","K2","E","H","E" };

        void Awake()
        {
            FC = GetComponent<FlyingController>();
        }

        void Start()
        {
            for (int i = 0; i < startingSections; i++)
            {
                GameObject structure = MultiplePooled.GetPooledObjectByName(GetNextSection(), structureGroup);
                structure.transform.position += new Vector3(0, 0, section_ID * sectionZLength);
                structure.SetActive(true);
                section_ID++;
            }
            endReached = false;
        }

        void Update()
        {
            if (!endReached)
            {
                SectionLeftLabel.text = "Sections Left : " + ((map.Length  * cycle) + startingSections - section_ID);
                SectionLeftLabel.color = Color.magenta;

                if (transform.position.z > (section_ID - startingSections) * 32)
                {
                    pushStructure();
                }
            }
        }

        private void pushStructure()
        {
            GameObject structure = MultiplePooled.GetPooledObjectByName(GetNextSection(), structureGroup);
            structure.transform.position += new Vector3(0, 0, section_ID * sectionZLength);
            structure.SetActive(true);
            section_ID++;
        }

        private string GetNextSection()
        {
            if (section_ID > (map.Length * cycle) + startingSections && FC.isAlive)
            {
                endReached = true;
                Invoke("GameWin", 5f);
                SectionLeftLabel.text = "Don't look back, you're not going that way...";
                SectionLeftLabel.color = Color.cyan;
            }

            return map[section_ID % map.Length];
        }

        public void GameWin()
        {
            audio.clip = VictorySound;
            audio.loop = false;
            audio.Play();
            Invoke("Quit", 2f);
 
        }

        public void Quit()
        {
            GameManager.instance.WinMinigame();
        }
    }
}