﻿using UnityEngine;
using System.Collections;

public class StartMusicAt : MonoBehaviour {
    
    public float MusicStartAt = 65.5f;

    void Start()
    {
        audio.time = MusicStartAt;
        audio.Play();
    }
}
