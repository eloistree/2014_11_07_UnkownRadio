﻿using UnityEngine;
using System.Collections;

public class Mouvement2 : MonoBehaviour {

	public float ratioF;
	public float ratioP;
	private float pasty=-1.0f;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(pasty < 0.5f)
			pasty=Theremin.instance.axis.y;


		float dy=(Theremin.instance.axis.y-pasty)/Time.deltaTime;
		pasty=Theremin.instance.axis.y;
		if(dy < 0){
			float forceup=-1*ratioF*dy;
			rigidbody.AddForce(0f,forceup,0f,ForceMode.Force);

		}
		float forcedown=-1*ratioP*(1-Theremin.instance.axis.y);
		rigidbody.AddForce(0f,forcedown,0f,ForceMode.Force);
	}
}
