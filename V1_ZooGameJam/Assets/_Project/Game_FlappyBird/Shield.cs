﻿using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour {

	public GameObject halo;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float t = Time.timeSinceLevelLoad;
		if(t>12){
			halo.SetActive(false);
		}else if(t<5){
			halo.SetActive(true);
		}else if(Mathf.Sin(4*Mathf.PI*Mathf.Pow(t-5,1.5f))>0){
			halo.SetActive(false);
		}else{
			halo.SetActive(true);
		}
	}
}
